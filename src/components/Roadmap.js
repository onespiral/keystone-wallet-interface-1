import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import BuildingBlockchain from '../assets/images/Building-Blockchain.jpg';
import KeystoneWallet from '../assets/images/Keystone-Wallet.jpg';
import Exchange from '../assets/images/Exchange.jpg';
import pic1 from '../assets/images/pic1.jpg';
import SmartContracts from '../assets/images/Smart-Contracts.jpg';

class Roadmap extends Component {
    render() {
      return (
  <div>
      <Header />


   <section>
   <div className="container-fluid" id="aboutus">
     <div className="container">
       <div className="row ">
         <div className="col text-center">
           <h1 className="green">The Future of Keystone</h1>
           <p className="color7">A roadmap of the future development of Keystone community.</p>
         </div>
       </div>
       <div className="m-5"></div>
       <div className="row ">
         <div className="col">
         <section id="timeline">
           <div className="demo-card-wrapper">
             <div className="demo-card">
               <div className="head color14-bg">
                 <div className="number-box">
                   <span>01</span>
                 </div>
                 <h2>Building Blockchain</h2>
               </div>
               <div className="body">
                 <p className="color17">Development completed of our highly secure, unique blockchain with private messaging </p>
                 <img src={BuildingBlockchain} alt="Graphic" />
               </div>
             </div>
             
             <div className="demo-card">
               <div className="head color16-bg">
                 <div className="number-box">
                   <span>02</span>
                 </div>
                 <h2>Keystone Wallet</h2>
               </div>
               <div className="body">
                 <p className="color18">Send and Receive Keystone </p>
                 <img src={KeystoneWallet} alt="Graphic" />
               </div>
             </div>

             <div className="demo-card ">
               <div className="head color8-bg">
                 <div className="number-box">
                   <span>03</span>
                 </div>
                 <h2>Exchange development</h2>
               </div>
               <div className="body">
                 <p className="color16">Ability to exchange with many different cryptocurrencies </p>
                 <img src={Exchange} alt="Graphic" />
               </div>
             </div>

             <div className="demo-card">
               <div className="head color10-bg">
                 <div className="number-box">
                   <span>04</span>
                 </div>
                 <h2>Smart Contracts</h2>
               </div>
               <div className="body">
                 <p className="color18">Contracts with Investors to build the La Tortuga community</p>
                 <img src={SmartContracts} alt="Graphic" />
               </div>
             </div>

             <div className="demo-card">
               <div className="head color6-bg">
                 <div className="number-box">
                   <span>05</span>
                 </div>
                 <h2>La Tortuga</h2>
               </div>
               <div className="body">
                 <p className="color17">Development of La Tortuga in Mexico with beach front Resorts, Casinos, natural resources & activities</p>
                 <img src={pic1} alt="Graphic" />
               </div>
             </div>

           </div>
         </section>
         </div>
       </div>
     </div>
   </div>
 </section>

   <Footer />
    </div>
    );
    }
  }
  export default withRouter(Roadmap);
