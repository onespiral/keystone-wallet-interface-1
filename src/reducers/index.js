import { combineReducers } from 'redux';
import AuthReducer from './auth';
import { reducer as FormReducer } from 'redux-form';
import UsersReducer from './users';
import TransactionsReducer from './transactions';
import BlockchainReducer from './blockchain';
import CirculationReducer from './circulation';
import CurrentBlockReducer from './currentBlock';
const rootReducer = combineReducers({
  auth: AuthReducer,
  form: FormReducer,
  users: UsersReducer,
  transaction: TransactionsReducer,
  chain: BlockchainReducer,
  circulation:CirculationReducer,
  currentBlock: CurrentBlockReducer
});

export default rootReducer;