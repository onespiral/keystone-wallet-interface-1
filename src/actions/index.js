import axios from 'axios';

import bcrypt from 'bcryptjs';

import Wallet from '../controllers/wallet';

import HOST_URL from '../configs';

export const CONNECTION_ERROR  = 'CONNECTION_ERROR';

export const USER_REGISTERED = 'USER_REGISTERED';

export const CHECK_IF_AUTHENTICATED = 'CHECK_IF_AUTHENTICATED';

export const USER_UNAUTHENTICATED = 'USER_UNAUTHENTICATED';

export const USER_AUTHENTICATED = 'USER_AUTHENTICATED';

export const AUTHENTICATION_ERROR = 'AUTHENTICATION_ERROR';

export const GET_USERS = 'GET_USERS';

export const MISSING_PARAMETER = 'MISSING_PARAMETER';

export const INVALID_BALANCE = 'INVALID_BALANCE';

export const INVALID_KEY = 'INVALID_KEY';

export const TRANSACTION_REJECTED = 'TRANSACTION_REJECTED';

export const TRANSACTION_RECORDED = 'TRANSACTION_RECORDED';

export const BLOCKCHAIN_ERROR = 'BLOCKCHAIN_ERROR';

export const BLOCKCHAIN_LAST_TEN = 'BLOCKCHAIN_LAST_TEN';

export const BLOCKCHAIN_LAST_FIFTEEN = 'BLOCKCHAIN_LAST_FIFTEEN';

export const BLOCKCHAIN_BY_ID = 'BLOCKCHAIN_BY_ID';

export const GET_CURRENT_BLOCK = 'GET_CURRENT_BLOCK';
export const GET_CIRCULATION = 'GET_CIRCULATION';

export const LOOKUP_ERROR = 'LOOKUP_ERROR';

export const authError = error => {
    return {
      type: AUTHENTICATION_ERROR,
      payload: error
    };
};
export const getCurrentBlock = () => {
  return dispatch => {
    axios
      .get(`${HOST_URL}/Block/Last-Block`)
      .then(response =>{
        return dispatch({
          type:GET_CURRENT_BLOCK,
          payload: response.data.blockId
        });
      })
      .catch(() => {
        return dispatch({
          type:LOOKUP_ERROR,
          payload: 0
        });
      });
  };
};

export const getCirculation = () => {
  return dispatch => {
      axios
          .get(`${HOST_URL}/Block/Circulation-Totals`)
          .then((response) => {
            if(response.status === 200){
              return dispatch({
                type:GET_CIRCULATION,
                payload:response.data[0].sum
              });
            } else {
              return dispatch({
                type:LOOKUP_ERROR,
                payload:'Error loading Circulation Totals'
              });
            }
          })
          .catch((err) => {
            return dispatch({
              type:LOOKUP_ERROR,
              payload: err[0]
            });
          });

  };
};


export const register = (privateKey, history) => {
    return dispatch => {
      if (!privateKey) {
        dispatch(authError('Private key is required to register your wallet.'));
        return;
      }
      axios
        .post(`${HOST_URL}/users`, { privateKey })
        .then(() => {
          dispatch({
            type: USER_REGISTERED
          });
          history.push('/');
        })
        .catch(() => {
          dispatch(authError('Failed to register private key'));
        });
    };
  };

  export const login = (privateKey, history) => {
    let tempWallet = new Wallet();
    if(privateKey !== null){
      tempWallet.setPrivateKey(privateKey.replace('\\n', '\n'));
      if (tempWallet.key.isPrivate()) {
        return dispatch => {
                dispatch({
                  type: USER_AUTHENTICATED,
                  payload: tempWallet
                });
                history.push('/Wallet');
            };
      } else {
        return dispatch => { dispatch(authError('Failed to load key.')); };
      }
    } else {
        return dispatch => { dispatch(authError('Failed to load. Key missing.')); };      
    }
  };

  export const logout = () => {
    return dispatch => {
      dispatch({
            type: USER_UNAUTHENTICATED
          });
    };
  };

 export const getBlockchain = () => {
    return dispatch => {
      axios
        .get(`${HOST_URL}/Block`) //{headers:{Authorization:token}}
        .then(response => {
          dispatch({
            type: BLOCKCHAIN_LAST_TEN,
            payload: response
          });
        })
        .catch((err) => {
          dispatch({
            type: BLOCKCHAIN_ERROR,
            payload : err
            });
        });
    };
 };

export const sendTransaction = (receiver, sender, amount = 0, stake, message, history) => {
    return dispatch => {
      if (!receiver || !sender ) {
        return dispatch({
            type: MISSING_PARAMETER,
            payload: "A parameter that was required is missing."
        });
      }
      const receiverWallet = new Wallet();
      try{
        receiverWallet.setPublicKey(receiver);
      } catch( err ){
        return dispatch({
            type: TRANSACTION_REJECTED,
            payload: err
        });
      }
      if(!receiverWallet.key.isPublic()){
        return dispatch({
            type: TRANSACTION_REJECTED,
            payload: "Invalid receiver wallet."
        });
      }

      if(message === undefined){
        message = null;
      }
      /* get spending transaction */
      const transaction = sender.signTransaction(amount, receiverWallet.walletPublicKey, stake, message, []);
      let validTransaction = {
        transactionsIndex: 0,
        listingHost : HOST_URL,
        forged:false,
        fee:0//get fee from listing node.
      };
      validTransaction = Object.assign(validTransaction, transaction);
      axios.get(`${HOST_URL}/Transaction/Last-Transaction`)
        .then(lastTransaction => {
          validTransaction.txPreviousHash = lastTransaction.data.transactions[0].txHash;
          validTransaction.transactionsIndex = lastTransaction.data.transactions[0].transactionsIndex + 1;
          let proof = 0;
          validTransaction.txHash = bcrypt.hashSync(JSON.stringify(validTransaction) + 3 + proof + 2, 1);
          console.log('Hash Before : ', validTransaction.txHash);
          while (validTransaction.txHash.substr(7, 2) !== 'KE') {
            proof++;
            validTransaction.txHash = bcrypt.hashSync(JSON.stringify(validTransaction) + 3 + proof + 2, 1);
            // Add this to the state
            console.log('Hash : ', validTransaction.txHash);
          }
          console.log('Hash After : ', validTransaction.txHash);
          axios
            .post(`${HOST_URL}/Transaction/New`, {transaction:validTransaction}, { headers:{ Authorization:sender.walletPublicKey.split('\n').join('\\n') } } )
            .then((response) => {
              console.log(response);
              dispatch({
                type: TRANSACTION_RECORDED,
                payload: response.data
              });
              history.push('/');
            })
            .catch((error) => {
              dispatch({
                type: TRANSACTION_REJECTED,
                payload: error
                });
            });
        })
        .catch((error) => {
          dispatch({
            type: CONNECTION_ERROR,
            payload: error
            });
        });
    };
  };
